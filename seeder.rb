require 'roo'



xlsx = Roo::Excelx.new(File.expand_path('../Downloads/UKPRN.xlsx'))

xlsx.each_row_streaming(offset: 1) do |row|
  Institute.find_or_create_by(ukprn: row[0].value, name: row[1].value)

end