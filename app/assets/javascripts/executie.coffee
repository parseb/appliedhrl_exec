# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


jQuery -> 
	$('#execs').dataTable({

	"language": {
                
    "sProcessing":   "Procesează...",
    "sLengthMenu":   "Afișează _MENU_ înregistrări pe pagină",
    "sZeroRecords":  "Nu am găsit nimic - ne pare rău",
    "sInfo":         "Afișate de la _START_ la _END_ din _TOTAL_ înregistrări",
    "sInfoEmpty":    "Afișate de la 0 la 0 din 0 înregistrări",
    "sInfoFiltered": "(filtrate dintr-un total de _MAX_ înregistrări)",
    "sInfoPostFix":  "",
    "sSearch":       "tc = Total Cheltuieli, tv = Total Venituri |  Caută:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "Prima",
        "sPrevious": "Precedenta",
        "sNext":     "Următoarea",
        "sLast":     "Ultima"
    }

            }
	}); 