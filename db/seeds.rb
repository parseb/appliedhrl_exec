# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


require 'csv'

csv_text = File.read(Rails.root.join('lib', 'seeds', 'hrl16.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'UTF-8')
csv.each do |row|
 # t = Executie.new
  

#tv&tc-total:belong to month. Bad seed. No total p.m. 

#  t.tv = row['tv']
#  t.tc = row['tc']


   e = Executie.find_or_create_by!(month: row['month'],
				  exec_cumul: row['exec_cumul'],
				  eco_desc: row['exo_desc'],
				  eco_code_class: row['eco_code_class'],
				  func_desc: row['func_desc'],
				  funct_code_class: row['funct_code_class'],
				  sursa: row['sursa'],
				  tip: row['tip'],
				  created_at: Time.now,
				  updated_at: Time.now)
 #  e.save 

#  puts "#{t.month}, #{t.tc} saved"
end

puts "There are now #{Executie.count} rows in the transactions table"


   
